import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { gettracksplaylist, getUserPlaylists } from '../../reducers/songsReducer';
import './styles.scss'

export const PlaylistCard = () => {
  const playlist = useSelector(state => state.songs.playlists);
  const dispatch = useDispatch();
  console.log(playlist)


  return (
    <section className='mainview'>
      <div className='mainview__container'>
        <h2>Your Playlists</h2>
        <div className='card__container animate__animated animate__fadeIn'>
            {playlist.map((album) => (
              <div className='mainview__card'
              onClick={()=>{
                dispatch(gettracksplaylist(album.id,album.name))                            
            }}>
                <figure className='Cover'>
                   <img className='cover__photo' src={album.image} />
                </figure>                
                <p>{album.name}</p>
              </div>
            ))
            }
        </div>




      </div>

    </section>
  )
}
