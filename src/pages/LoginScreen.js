import React, { useEffect, useState } from 'react';
import axios from 'axios';


export const LoginScreen = () => {
    const CLIENT_ID='d8a5862b254f4aa588504e51c4034323';
    const REDIRECT_URI='http://localhost:3000/'
    const AUTH_ENDPOINT='https://accounts.spotify.com/authorize'
    const RESPONSE_TYPE='token'

    const [token,setToken]=useState('')
    useEffect(() => {
      const hash=window.location.hash;
      let token= window.localStorage.getItem('token');

      if(!token && hash){
          token=hash.substring(1).split('&').find(elem=>elem.startsWith('access_token')).split('=')[1]    
          
          window.location.hash=''
          window.localStorage.setItem('token',token)
        }
        
        setToken(token);
    }, [])
    
    const logout =()=>{
        setToken('')
        window.localStorage.removeItem('token') 
        
    }

     const gettracksplaylist = async() => {
        const {data}=await axios.get(`https://api.spotify.com/v1/playlists/5yxPwSLYZcleiYOpc5vlh8/tracks?limit=10`,
           {
               headers: {
                   'Authorization': `Bearer ${token}`
               }
            })
            console.log(data)
   }

    const getUserPlaylists = async () => {
        const {data}=await axios.get('https://api.spotify.com/v1/users/22g5pe4pbf2dl6gfw7db74ada/playlists?offset=0&limit=10',
        {
            headers: { Authorization: `Bearer ${token}` }
        })
        console.log(data)
        

}
    


  return (
    <div className='App'>
        <h1>LoginScreen</h1>
        {!token?
            <a href={`${AUTH_ENDPOINT}?client_id=${CLIENT_ID}&redirect_uri=${REDIRECT_URI}&response_type=${RESPONSE_TYPE}`}>Login to Spotify</a>
            : <button onClick={logout}>Logout</button>&&<button onClick={getUserPlaylists}>data</button>
        }

        
    </div>
  )
  
}
