import React from 'react'
import { Sidebar } from '../../components/organism/sidebar/Sidebar'
import { PlaylistInfo } from '../../components/templates/playlistinfo/PlaylistInfo'

export const PlaylistIdScreen = () => {
  
  return (
    <div className='App__main__content'>
      <Sidebar />
      <main>
        <PlaylistInfo/>
      </main>
    </div>
  )
}
