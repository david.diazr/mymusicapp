import React from 'react'
import { useSelector } from 'react-redux';
import { Header } from '../../components/organism/header/Header'
import { Sidebar } from '../../components/organism/sidebar/Sidebar'
import { Topinfo } from '../../components/organism/topInfo/TopInfo';
import { SongsRow } from '../../components/templates/songsrow/SongsRow';
import './styles.scss';

export const FavoriteScreen = () => {
    const favorites = useSelector( state => state.songs.favoritesongs );
    return (
        <div className='mainContent'>
            <Sidebar />
            <div className='favorites'>
                <Header />
                <Topinfo text={'Your Favorite songs'} length={favorites.length}/>
                <SongsRow tracks={favorites}/>
                <div />
            </div>
        </div>
    )
}
