import React from 'react'
import { Sidebar } from '../../components/organism/sidebar/Sidebar'
import { SongScreen } from '../../components/organism/songScreen/SongScreen'
import './styles.scss'

export const HomeScreen = () => {
  return (
    <div className='App__main__content'>
        <Sidebar/>
        <main>
          <SongScreen/>
        </main>
    </div>
  )
}
