export const types = {

    getAllSongOfPlaylist: '[Playlist] Get all songs',
    getFavoriteSongs: '[Playlist] Get all favorite songs',
    removeSongs: '[Playlist] remove songs',
}