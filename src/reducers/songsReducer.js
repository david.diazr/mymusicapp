import { createSlice } from "@reduxjs/toolkit"
import axios from 'axios';

const token = localStorage.getItem('token');

export const songSlice = createSlice({
    name: "songs",
    initialState: {
        playlists: [],
        tracks: [],
        favoritesongs: [],
    },
    reducers:
    {
        setPlaylistSongs: (state, action) => {
            state.playlists = action.payload;
        },

        setPlaylistId: (state, action) => {
            state.tracks = action.payload;
        },

        setFavoriteSong:(state,action)=>{
            state.favoritesongs= [...state.favoritesongs,action.payload];
        },

        removeFavoriteSong:(state,action)=>{
            state.favoritesongs= state.favoritesongs.filter(
                (song)=> song.id !== action.payload
            );
        },
        resetState:(state)=>{
            state.tracks=[];
        }

        // setPlaylistinfo:(state,action)=>{
        //     state.playlists=

        // }
    }
});

export const getUserPlaylists = () => (dispatch) => {
    axios.get('https://api.spotify.com/v1/users/22g5pe4pbf2dl6gfw7db74ada/playlists?offset=0&limit=10',
        {
            headers: { Authorization: `Bearer ${token}` }
        })
        .then(({ data }) => {
            dispatch(setPlaylistSongs(data.items.map(items => {
                return { 
                    id: items.id,
                    name: items.name,
                    image:items.images[0].url
                 }
            })));
        }).catch((error) => {
            console.log('Error', 'ERror to load songs from spotify')
        })

}

export const gettracksplaylist = (id,name) => (dispatch) => {
     axios.get(`https://api.spotify.com/v1/playlists/${id}/tracks?limit=10`,
        {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        }).then(({ data }) => {
            dispatch(setPlaylistId(data.items.map(items => {
                return {
                    name: items.track.name,
                    duration: items.track.duration_ms,
                    album:items.track.album.name,
                    image:items.track.album.images[1].url, 
                    id:items.track.id,
                    playlistname:name,
                }
            }
            )))
            
            ;
        }).catch((error) => {
            console.log('Error', 'ERror to load songs from spotify aaaaa')
        })
}

export const addFavoriteSongs=(tracks)=>(dispatch)=>{
    dispatch(setFavoriteSong(tracks));
}



export const { setPlaylistSongs,setPlaylistId,setFavoriteSong,removeFavoriteSong,resetState } = songSlice.actions;
export default songSlice.reducer;