import { AppRouter } from './routers/AppRouter';
export const App = () => {
  return (
    <div className="App">
        <AppRouter/>
    </div>
  );
}
