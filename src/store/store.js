import {configureStore} from '@reduxjs/toolkit'
//reducers
import songs from '../reducers/songsReducer'; 

export default configureStore({
    reducer:{
        songs
    }
});
