import React from 'react'
import { Routes, Route,BrowserRouter } from "react-router-dom";
import { Botones } from '../Botones';
import { FavoriteScreen } from '../pages/favorites/FavoriteScreen';
import { HomeScreen } from '../pages/home/HomeScreen';
import { LoginScreen } from '../pages/LoginScreen';
import { PlaylistIdScreen } from '../pages/playlistid/PlaylistIdScreen';




export const AppRouter = () => {
  return (
    <BrowserRouter>
    <Routes>
      <Route path="/login" element={<HomeScreen/>} />
      <Route path="/" element={<LoginScreen/>} />
      <Route path="/favorites" element={<FavoriteScreen/>} />
      <Route path='/songs/:playlistid' element={<PlaylistIdScreen/>}/>
    </Routes>
  </BrowserRouter>
  )
}
