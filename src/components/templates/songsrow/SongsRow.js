import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Songsinfo } from './Songsinfo'
import './styles.scss'
export const SongsRow = ({tracks}) => {

  return (
    <div className='itemsong'>
      <div className='info__row'>
        <div>
          <span>#</span>
        </div>

        <div className='title__column'>
          <span className='title'>Título</span>
        </div>

        <div className='title__column'>
          <span>Albúm</span>
        </div>
        <i className="bi bi-heart fav" ></i>
      </div>

      <hr />

      {        
        tracks.length>0 && tracks.map((songs) => (
          <Songsinfo key={songs.id} name={songs.name} album={songs.album} image={songs.image} duration={songs.duration} id={songs.id} />
        ))
      }

    </div>

  )
}
