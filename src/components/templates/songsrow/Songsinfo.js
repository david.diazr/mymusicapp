import React from 'react'
import './styles.scss'
import { useDispatch, useSelector } from 'react-redux'
import { addFavoriteSongs, removeFavoriteSong } from '../../../reducers/songsReducer';
import { ImageSong } from '../../atoms/imageSong/ImageSong';
import { TextSongs } from '../../atoms/textSongs/TextSongs';

export const Songsinfo = (songs) => {
    const favorites = useSelector( state => state.songs.favoritesongs );
    const dispatch = useDispatch();
    const handleAddOrRemoveFavorites=(a)=>{
        console.log(favorites.filter((song)=>song.id !== a)) 
    
      }

  return (
    <div className='m-itemSong'>
            <div className='m-itemSong__item '>
                <ImageSong url={songs.image} />
                <div className='m-itemSong__item'>
                <TextSongs text={songs.album} />
                <TextSongs text={songs.name} />
            </div>
            <div className='m-itemSong__item m-itemSong__item--dissapear'>
                 <TextSongs text={songs.album} />
            </div>
            <div className='m-itemSong__item m-itemSong__item--favorite' >

                { (favorites.map(tracks=>tracks.id).indexOf(songs.id)!==-1) ?
                  <i className="bi bi-heart-fill fav" onClick={()=> dispatch(removeFavoriteSong(songs.id))}></i>
                  :
                  <i className="bi bi-heart fav" onClick={()=>  dispatch(addFavoriteSongs(songs))} ></i>
                }

              </div>
              <div className='m-itemSong__item'>
                    <TextSongs text={songs.duration} />
              </div>

            </div>

          </div>
  )
}
