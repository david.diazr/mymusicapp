import React from 'react'
import { useSelector } from 'react-redux'
import { PlaylistCard } from '../../../pages/playlistCard/PlaylistCard'
import { SongsRow } from '../songsrow/SongsRow'
import './styles.scss'

export const PlaylistInfo = ({text, favorites}) => {

    const id = useSelector( state => state.songs.tracks );
    const tracks = useSelector(state => state.songs.tracks);
    
    return (
        <div>

           {
        id.length!==0 ?
            <div className='main-view'>
               <div className='contentSpacing'>
                <figure >
                    <img className='cover' src='https://i.scdn.co/image/ab67616d0000b273cf7e54f668d6a31dd6566f24' alt='cover_image' />
                </figure>

                <div className='content__info'>
                    <h1 className='playlist__title'>{(text!==undefined) ? text: id[0].playlistname}</h1> 
                    <a>Juan David Aizcream</a>
                    <a>{`${id.length} canciones`}</a>
                </div>
            </div>
                <SongsRow tracks={tracks}/>
        </div>
            
            :

            <PlaylistCard/>
            
        }
        
        </div>
    )
}
