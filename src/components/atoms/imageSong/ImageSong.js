import React from 'react'
import './styles.scss'

export const ImageSong = ({url, name}) => {
    return (
        <figure>
            <img className='a-imageSong' src={url} alt={name} />
        </figure>
    )
}
