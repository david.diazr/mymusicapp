import React from 'react'
import "./Styles.scss";

export const Button = ({buttonType,label,classButton,clickEvent}) => {
  return (
    <button name={label.toLocaleLowerCase()} type={buttonType} className={`a-button ${classButton}`} onClick={clickEvent}>
        <span>{ label }</span>
    </button>
  )
}

