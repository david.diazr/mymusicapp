import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Link, Navigate, useNavigate } from 'react-router-dom'
import { gettracksplaylist, getUserPlaylists, resetState } from '../../../reducers/songsReducer'
import './styles.scss'

export const Sidebar = () => {
    const dispatch = useDispatch();
    const  playlist  = useSelector(state => state.songs.playlists);
    const tracks = useSelector( state => state.songs.tracks );
    const navigate=useNavigate();
    
    useEffect(() => {
        dispatch(getUserPlaylists());
        console.log(playlist)
    }, [dispatch])
    
    const handleFavorites=()=>{
        navigate('/favorites')
    }
    const handleHome=()=>{
        navigate('/login')
    }


    return (
        
        <aside className='aside__menu'>
            <figure className='aside__header'>
                <img
                    className='logo'
                    src='https://storage.googleapis.com/pr-newsroom-wp/1/2018/11/Spotify_Logo_CMYK_White.png'
                    onClick={()=>{dispatch(resetState(tracks))
                    handleHome()
                    }}

                />
            </figure>

            <ul>
                <li className='list__item'>
                    <div className='aside__options' 
                        onClick={()=>{dispatch(resetState(tracks))
                        handleHome()
                        }}>
                        <i className="bi bi-house-door icon"></i>
                        <span >Home</span>
                    </div>

                </li>

                <li className='list__item'>
                    <div className='aside__options' onClick={handleFavorites}>
                        <i className="bi bi-heart-fill icon"></i>
                        <span>Your Favorites</span>
                    </div>
                </li>
            </ul>

            <hr />

            <ul>
                {
                    playlist.map((playlists, index) => (
                        <li 
                        key={index}
                        className='list__item'
                        onClick={()=>{
                            dispatch(gettracksplaylist(playlists.id,playlists.name))                            
                        }}>
                            <Link className='aside__playlists' to={`/songs/${playlists.id}`}>                     
                                <span>
                                    {playlists.name}</span>
                            </Link>
                        </li>
                    ))
                }
            </ul>
        </aside>
    )
}
