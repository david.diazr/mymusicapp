import React from 'react'
import './styles.scss'


export const Topinfo = ({text,length}) => {
  return (
    <div className='contentSpacing'>
        <figure >
            <img className='cover' src='https://t.scdn.co/images/3099b3803ad9496896c43f22fe9be8c4.png' alt='cover_image' />
         </figure>

         <div className='content__info'>
            <h1 className='playlist__title'>{text}</h1> 
            <a>Juan David Diaz</a>
            <a>{`${length} Songs`}</a>
        </div>
    </div>
  )
}
