import React from 'react'
import './styles.scss'

export const Header = () => {
    return (
        <header className='song__header'>
            <p>Mejora tu cuenta</p>
            <button className='user__info'>
                <figure className='profile__picture'>
                    <img className='profile__picture'
                        src='https://scontent-bog1-1.xx.fbcdn.net/v/t1.18169-9/27867813_1820214544942612_2578286788725586629_n.jpg?_nc_cat=104&ccb=1-6&_nc_sid=174925&_nc_ohc=eDE3WkQh0PQAX_Be4kP&tn=p_T7Jxkfn1REGH5M&_nc_ht=scontent-bog1-1.xx&oh=00_AT-a1IbCd_1LaGomx5-qQ7A3BIlq-xY4Czoayo-VjjBruQ&oe=62A2B085'
                    />
                </figure>
                <span className='text'>Juan David Díaz</span>
                <i className="bi bi-caret-down-fill mr"></i>
            </button>
        </header>

    )
}
