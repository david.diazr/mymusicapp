import React from 'react'
import { PlaylistInfo } from '../../templates/playlistinfo/PlaylistInfo'
import { Header } from '../header/Header'
import './styles.scss'

export const SongScreen = () => {
  return (
    
    <div className='song__main-content'>
      <Header/>
        <div className='song__content'>
            <PlaylistInfo/>
        </div>


    </div>
  )
}
